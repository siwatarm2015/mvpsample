package com.blacklenspub.starwarsapp.model

class Film {
    var episodeId: Long = 0
    var title: String? = null
    var releaseDate: String? = null
    var director: String? = null
    var openingCrawl: String? = null
}
