package com.blacklenspub.starwarsapp.home

import com.blacklenspub.starwarsapp.model.Film

interface HomeContract {
    interface HomeView {
        fun showLoading()

        fun hideLoading()

        fun showTitle(title: String)

        fun showMessage(message: String?)

        fun showAllFilms(films: List<Film>)

        fun navigateToFilmPage(film: Film)
    }

    interface HomePresenter {
        fun getAllFilms()

        fun onFilmItemClicked(film: Film)
    }
}
