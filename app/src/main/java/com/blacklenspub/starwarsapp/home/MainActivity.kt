package com.blacklenspub.starwarsapp.home

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.blacklenspub.starwarsapp.R
import com.blacklenspub.starwarsapp.film.FilmActivity
import com.blacklenspub.starwarsapp.model.Film
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), HomeContract.HomeView,FilmAdapter.OnFilmClickListener {
    override fun onFilmClick(film: Film?) {
        presenter.onFilmItemClicked(film!!)
    }

    private lateinit var filmAdapter: FilmAdapter
    private lateinit var presenter: HomeContract.HomePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        filmAdapter = FilmAdapter()
        filmAdapter.onFilmClickListener = this
        rvFilms.layoutManager = LinearLayoutManager(this)
        rvFilms.adapter = filmAdapter

        srl.setOnRefreshListener { presenter.getAllFilms() }

        presenter = HomePresenter(this)
        presenter.getAllFilms()
    }

    override fun showLoading() {
        srl.isRefreshing = true
    }

    override fun hideLoading() {
        srl.isRefreshing = false
    }

    override fun showTitle(title: String) {
        setTitle(title)
    }

    override fun showMessage(message: String?) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun showAllFilms(films: List<Film>) {
        filmAdapter.setFilms(films)
    }

    override fun navigateToFilmPage(film: Film) {
        FilmActivity.start(this@MainActivity, film.episodeId)
    }
}