package com.blacklenspub.starwarsapp.home

import com.blacklenspub.starwarsapp.model.Apis
import com.blacklenspub.starwarsapp.model.Film
import com.blacklenspub.starwarsapp.model.FilmResponse
import com.blacklenspub.starwarsapp.model.StarWarsApi

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomePresenter(private val view: HomeContract.HomeView) : HomeContract.HomePresenter {

    private val starWarsApi: StarWarsApi = Apis.getSwarWarsApi()

    init {
        view.showTitle("All Star Wars Films")
    }

    override fun getAllFilms() {
        view.showLoading()
        starWarsApi.allFilms.enqueue(object : Callback<FilmResponse> {
            override fun onResponse(call: Call<FilmResponse>, response: Response<FilmResponse>) {
                view.showAllFilms(response.body()!!.results)
                view.hideLoading()
            }

            override fun onFailure(call: Call<FilmResponse>, t: Throwable) {
                view.showMessage(t.message)
                view.hideLoading()
            }
        })
    }

    override fun onFilmItemClicked(film: Film) {
        view.navigateToFilmPage(film)
    }
}
