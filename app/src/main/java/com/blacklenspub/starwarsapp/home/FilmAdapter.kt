package com.blacklenspub.starwarsapp.home

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.blacklenspub.starwarsapp.R
import com.blacklenspub.starwarsapp.model.Film

class FilmAdapter : RecyclerView.Adapter<FilmAdapter.FilmHolder>() {

    private var films: List<Film>? = null
    var onFilmClickListener: OnFilmClickListener? = null

    fun setFilms(films: List<Film>) {
        this.films = films
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilmHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_film, parent, false)
        return FilmHolder(view)
    }

    override fun onBindViewHolder(holder: FilmHolder, position: Int) {
        val item = films!![position]
        holder.film = item
        holder.title.text = item.title
    }

    override fun getItemCount(): Int {
        return if (films == null) {
            0
        } else {
            films!!.size
        }
    }

     inner class FilmHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var film: Film? = null
        var title: TextView = itemView.findViewById<View>(R.id.tvTitle) as TextView

         init {
             itemView.setOnClickListener { onFilmClickListener?.onFilmClick(film) }
        }
    }

     interface OnFilmClickListener {
        fun onFilmClick(film: Film?)
    }
}
