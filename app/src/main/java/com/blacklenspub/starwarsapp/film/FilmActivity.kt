package com.blacklenspub.starwarsapp.film

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.blacklenspub.starwarsapp.R
import kotlinx.android.synthetic.main.activity_film.*

class FilmActivity : AppCompatActivity(), FilmContract.FilmView {

    companion object {

        private const val KEY_FILM_ID = "FILM_ID"

        fun start(context: Context, episodeId: Long) {
            val starter = Intent(context, FilmActivity::class.java)
            starter.putExtra(KEY_FILM_ID, episodeId)
            context.startActivity(starter)
        }
    }

    private lateinit var presenter: FilmPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_film)
        val episodeId = intent.getLongExtra(KEY_FILM_ID, 0)

        presenter = FilmPresenter(this, episodeId)
        presenter.getFilm()
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
    }

    override fun showMessage(message: String?) {
        Toast.makeText(this@FilmActivity, message, Toast.LENGTH_SHORT).show()
    }

    override fun showTitle(title: String) {
        setTitle(title)
    }

    override fun showReleaseDate(dateString: String) {
        tvReleaseDate.text = dateString
    }

    override fun showDirector(director: String) {
        tvDirector.text = director
    }

    override fun showCrawl(crawl: String) {
        tvCrawl.text = crawl
    }
}
