package com.blacklenspub.starwarsapp.film

import com.blacklenspub.starwarsapp.model.Apis
import com.blacklenspub.starwarsapp.model.Film
import com.blacklenspub.starwarsapp.model.StarWarsApi

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FilmPresenter(private val view: FilmContract.FilmView, private val id: Long) : FilmContract.FilmPresenter {

    private val starWarsApi: StarWarsApi = Apis.getSwarWarsApi()

    override fun getFilm() {
        view.showLoading()
        starWarsApi.getFilm(id).enqueue(object : Callback<Film> {
            override fun onResponse(call: Call<Film>, response: Response<Film>) {
                val film = response.body()
                view.showTitle(film!!.title!!)
                view.showReleaseDate(film.releaseDate!!)
                view.showDirector(film.director!!)
                view.showCrawl(film.openingCrawl!!)
                view.hideLoading()
            }

            override fun onFailure(call: Call<Film>, t: Throwable) {
                view.showMessage(t.message)
                view.hideLoading()
            }
        })
    }
}
