package com.blacklenspub.starwarsapp.film

interface FilmContract {
    interface FilmView {
        fun showLoading()

        fun hideLoading()

        fun showMessage(message: String?)

        fun showTitle(title: String)

        fun showReleaseDate(dateString: String)

        fun showDirector(director: String)

        fun showCrawl(crawl: String)
    }

    interface FilmPresenter {
        fun getFilm()
    }
}
